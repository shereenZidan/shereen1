<?php

    // configuration
    require("../includes/config.php");

    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        render("updateProduct.php", ["title" => "update product"]);
    }

    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["name"]))
        {
            apologize("You must enter a name.");
        }
        else if (empty($_POST["description"]))
        {
            apologize("You must enter a description.");
        }
        else if (empty($_POST["price"]))
        {
            apologize("You must enter a price.");
        }
        else if (empty($_POST["quantity"]))
        {
            apologize("You must enter quantity.");
        }
        
        else
        {
            $name=$_POST["name"];
            $description=$_POST["description"];
            $price=$_POST["price"];
            $quantity=$_POST["quantity"];
            if(isset($_POST['submit'])) 
            {
                $category = $_POST['product'];
                $updated=$_POST['selected'];
            }
             $result= query("UPDATE products SET name = '$name', description= '$description', price= '$price', quantity= '$quantity', category_id= '$category' WHERE id=$updated ");
            if($result)
            {
                redirect("/");
            }
            else
            {
                echo"query is not right!";
            }
        }
    }

?>
