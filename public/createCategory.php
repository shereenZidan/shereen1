<?php

    // configuration
    require("../includes/config.php");

    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        render("createCategory.php", ["title" => "create category"]);
    }

    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["name"]))
        {
            apologize("You must enter a name.");
        }
        else if (empty($_POST["description"]))
        {
            apologize("You must enter a description.");
        }
        else
        {
            $name=$_POST["name"];
            $description=$_POST["description"];
            $result= query("INSERT INTO categories (name,description) VALUES ('$name', '$description')");
            if($result)
            {
                redirect("/");
            }
            else
            {
                echo"query is not right!";
            }
        }
    }

?>
