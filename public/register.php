<?php

    // configuration
    require("../includes/config.php");

    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        // else render form
        render("register.php", ["title" => "Register"]);
    }

    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["username"]))
        {
            apologize("You must enter a username.");
        }
        else if (empty($_POST["password"]))
        {
            apologize("You must enter a password.");
        }
        else if (empty($_POST["confirmation"]))
        {
            apologize("You must confirm your password.");
        }
        else if ($_POST['password'] != $_POST['confirmation']) {
            apologize("Your password and password confirmation do not match.");
        }
        else
        {
            $username = $_POST["username"];
            $hash = password_hash($_POST["password"], PASSWORD_DEFAULT);
            $result = query("INSERT IGNORE INTO users (username, hash) VALUES('$username', '$hash')");
            $row = query("SELECT id FROM users ORDER BY id DESC LIMIT 1;");
            $id = $row[0]['id'];
            $_SESSION["id"] = $id;
            redirect("/");
        }
    }

?>
