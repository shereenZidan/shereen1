
<table class="infotable">
<?php foreach ($categories as $category): ?>
    <tr>
        <td>
          <a href="/category.php?categoryId=<?= $category['id'] ?>">
            <?= $category["name"] ?>
          </a>
        </td>
        <td><?= $category["description"] ?></td>
        <td>
          <a href="/updateCategory.php">Edit</a>
        </td>
    </tr>

<?php endforeach ?>
</table>
